<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Depositions extends Model
{
    protected $fillable = [
        'name',
        'role',
        'quote',
        'file_path'
    ];
}
