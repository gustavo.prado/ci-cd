<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\DepositionRequest;
use App\Depositions;
use Illuminate\Support\Facades\Storage;

class DepositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'Success',
            'data' => Depositions::all()
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepositionRequest $request)
    {
        $file_path = $request->file('file')->store('depositions');

        $deposition = Depositions::create([
            'name' => $request->name,
            'role' => $request->role,
            'quote' => $request->quote,
            'file_path' => $file_path
        ]);

        return response()->json([
            'message' => 'Success',
            'data' => $deposition
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Depositions $deposition)
    {
        return response()->json([
            'message' => 'Success',
            'data' => $deposition
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Depositions $deposition)
    {
        $file_path = NULL;

        if($request->file('file')){
            if(Storage::exists($deposition->file_path)) {
                Storage::delete($deposition->file_path);
            }
            $file_path = $request->file('file')->store('depositions');
        }

        $deposition->update([
            "name" => $request->name,
            "role" => $request->role,
            "quote" => $request->quote,
            "file_path" => $file_path ? $file_path : $deposition->file_path
        ]);

        return response()->json([
            'message' => 'Success',
            'data' => $deposition
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Depositions $deposition)
    {
        if(Storage::exists($deposition->file_path)){
            Storage::delete($deposition->file_path);
        }

        $deposition->delete();

        return response()->json([
            'message' => 'Success'
        ], 200);
    }
}
