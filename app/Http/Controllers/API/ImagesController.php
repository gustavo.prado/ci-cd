<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ImagesRequest;
use Illuminate\Support\Facades\Storage;
use App\Images;
use App\Service;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'Success',
            'data' => Images::all()
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImagesRequest $request)
    {
        $file_path = $request->file('file')->store('images');

        $image = Images::create([
            'title' => $request->title,
            'file_path' => $file_path
        ]);

        return response()->json([
            'message' => 'Success',
            'data' => $image
        ],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Images $image)
    {
        return response()->json([
            'message' => 'Success',
            'data' => $image,
            'icon' => $image->icon,
            'image' => $image->image
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ImagesRequest $request, Images $image)
    {
        $file_path = NULL;

        if($request->file('file')){
            if(Storage::exists($image->file_path)){
                Storage::delete($image->file_path);
            }
            $file_path = $request->file('file')->store('images');
        }

        $image->update([
            'title' => $request->title,
            'file_path' => $file_path ? $file_path : $image->file_path,
            'service_id' => $request->service_id
        ]);

        return response()->json([
            'message' => 'Success',
            'data' => $image
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Images $image)
    {
        if($image->icon != NULL || $image->image->isNotEmpty()){
            return response()->json([
                'message' => 'Cannot Delete'
            ],400);
        }
        else{
            if(Storage::exists($image->file_path)){
                Storage::delete($image->file_path);
            }

            $image->delete();

            return response()->json([
                'message' => 'Success',
            ],200);
        }
    }
}
