<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use App\Images;
use Illuminate\Support\Collection;
use App\Http\Requests\ServiceRequest;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'Success',
            'data' => Service::all()
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        if(!Images::find($request->icon_id) || !Images::find($request->images_id)){
            return response()->json([
                'message' => 'Invalid foreign key',
            ],400);
        }
        else{
            $service = Service::create([
                'name' => $request->name,
                'description' => $request->description,
                'icon_id' => $request->icon_id,
                'images_id' => $request->images_id
            ]);

            return response()->json([
                'message' => 'Success',
                'data' => $service
            ],201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return response()->json([
            'message' => 'Success',
            'data' => $service
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, Service $service)
    {
        $service->update($request->all());
        return response()->json([
            'message' => 'Success',
            'data' => $service
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();

        return response()->json([
            'message' => 'Success'
        ],200);
}
}
