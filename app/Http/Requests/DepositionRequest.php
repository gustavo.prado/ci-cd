<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepositionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'role' => 'required|string|max:255',
            'quote' => 'required|string|max:255',
            'file' => 'required|file|mimes:jpg,jpeg,bmp,png,webp|max:2048'
        ];
    }
}
