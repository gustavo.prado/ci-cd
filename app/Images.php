<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;
use Illuminate\Support\Collection;

class Images extends Model
{
    protected $fillable = [
        'title',
        'file_path',
    ];

    public function icon(){
        return $this->hasOne(Service::class, 'icon_id');
    }

    public function image(){
        return $this->hasMany(Service::class, 'images_id');
    }
}