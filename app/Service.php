<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Images;

class Service extends Model
{
    protected $fillable = [
        'name',
        'description',
        'icon_id',
        'images_id'
    ];

    public function icon(){
        return $this->belongsTo(Images::class);
    }

    public function image(){
        return $this->belongsTo(Images::class);
    }
}
