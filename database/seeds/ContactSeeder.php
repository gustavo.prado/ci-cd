<?php

use Illuminate\Database\Seeder;
use App\Contact;
use Illuminate\Support\Str;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //cria 11 contatos com mensagens e valores random
        for($i = 0; $i < 10; $i++){
            Contact::create([
                'name' => Str::random(10),
                'email' => Str::random(10),
                'subject' => Str::random(10),
                'message' => Str::random(25),
            ]);
        }
    }
}
