<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ImagesController;
use App\Http\Controllers\API\DepositionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
    Rotas de login.
*/
Route::post('login', 'API\AuthController@login');
Route::post('signup', 'API\AuthController@signup');

Route::apiResource('employee', 'API\EmployeeController');
Route::apiResource('contact', 'API\ContactController');
Route::apiResource('images', 'API\ImagesController');
Route::apiResource('depositions', 'API\DepositionController');
Route::apiResource('service', 'API\ServiceController');

/* rotas para user sem admin */
// Route::apiResource('employee', 'API\EmployeeController')->only(['show', 'index']);
// Route::apiResource('contact', 'API\ContactController')->only(['show', 'index']);
// Route::apiResource('images', 'API\ImagesController')->only(['show', 'index']);
// Route::apiResource('depositions', 'API\DepositionController')->only(['show', 'index']);
// Route::apiResource('service', 'API\ServiceController')->only(['show', 'index']);

/* 
    Rotas de autenticação.
*/
Route::middleware(['auth:api'])->group(function () {
    Route::middleware(['admin'])->group(function () {
        /*rotas de admin*/
        // Route::apiResource('employee', 'API\EmployeeController')->except(['show', 'index']);
        // Route::apiResource('contact', 'API\ContactController')->except(['show', 'index']);
        // Route::apiResource('images', 'API\ImagesController')->except(['show', 'index']);
        // Route::apiResource('depositions', 'API\DepositionController')->except(['show', 'index']);
        // Route::apiResource('service', 'API\ServiceController')->except(['show', 'index']);
    });

    Route::get('logout', 'API\AuthController@logout');
    Route::get('user', 'API\AuthController@user');

});